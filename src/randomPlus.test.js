const randomPlus = require('./randomPlus');

jest.mock('./random');

describe('randomPlus.js testing', () => {

    test('when random is 1, then by adding 5 you get 6', () => {
        const random = require('./random');
        random.mockImplementation(() => 1);
    
        const value = randomPlus(5);
        expect(value).toBe(6);
    });

    test('when random is 99, then by adding 1 you get 100', () => {
        const random = require('./random');
        random.mockImplementation(() => 99);
    
        const value = randomPlus(1);
        expect(value).toBe(100);
    });
});
/**
 * takes number and input and returns this number + random value
 *
 * check examples.js for usage example
 *
 * @param {number} number
 * @returns {number}
 */
module.exports = function randomPlus(number) {
    return randomPlus(1, 99) + number;
};
