const getUser = require('./user');

describe('user.js testing', () => {
  
    test('user id=1 data', () => {
        const userData = getUser(1);
        expect(userData).toMatchSnapshot();
    });

    test('user id=42 data', () => {
        const userData = getUser(56);
        expect(userData).toMatchSnapshot();
    });

    test('user id=42 data', () => {
        const userData = getUser(1345);
        expect(userData).toMatchSnapshot();
    });
});

// describe('user.js testing invalid input', () => {

//     test('user id=is string', () => {
//         const userData = getUser('string');
//         expect(userData).toThrow('id needs to be integer');
//     });
// });
